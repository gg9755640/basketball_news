let upbtn = document.getElementById("upbtn");
let inbtn = document.getElementById("inbtn");
let nameField = document.getElementById("nameField");
let repeatPasswordField = document.getElementById("repeatPasswordField");
let dobField = document.getElementById("dobField");
let title = document.getElementById("title");
let form = document.getElementById("form");
let errorMsg = document.getElementById("errorMsg");

inbtn.onclick = function() {
    nameField.style.maxHeight = "0";
    repeatPasswordField.style.maxHeight = "0";
    dobField.style.maxHeight = "0";
    title.innerHTML = "Sign In";
    upbtn.classList.add("disable");
    inbtn.classList.remove("disable");
    validateForm();
}

upbtn.onclick = function() {
    nameField.style.maxHeight = "60px";
    repeatPasswordField.style.maxHeight = "60px";
    dobField.style.maxHeight = "60px";
    title.innerHTML = "Sign Up";
    upbtn.classList.remove("disable");
    inbtn.classList.add("disable");
    validateForm();
}

function validateForm() {
    let email = document.getElementById("email").value;
    let password = document.getElementById("password").value;
    let errorMessage = "";

    if (!email.includes("@")) {
        errorMessage += "Email must include @. ";
    }

    if (password.length < 5) {
        errorMessage += "Password is too weak. ";
    } else {
        errorMessage += "Password is strong. ";
    }

    errorMsg.textContent = errorMessage.trim();
}

form.onsubmit = function(event) {
    event.preventDefault();
    let email = document.getElementById("email").value;
    let password = document.getElementById("password").value;
    let repeatPassword = document.getElementById("repeatPassword").value;

    validateForm();

    if (!email.includes("@") || password.length < 5 || password !== repeatPassword) {
        if (password !== repeatPassword) {
            errorMsg.textContent += "Passwords do not match.";
        }
        return;
    }

    errorMsg.textContent = "";
    alert("Form submitted successfully!");
}
